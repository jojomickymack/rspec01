class CoolThing
    def do_something
        'cool thing'
    end

    def add_numbers(num1, num2)
        num1 + num2
    end
end

describe CoolThing do
    #-> instatiate the object, check what this function returns
    context 'When testing the HelloWorld class' do
        it 'should say \'cool thing\' when we call the say_hello method' do
            expect(CoolThing.new().do_something()).to eq 'cool thing'
        end
    end

    #-> adding some numbers
    context 'When adding two and two' do
        it 'should equal four' do
            expect(CoolThing.new().add_numbers(2, 2)).to eq 7
        end
    end

    #-> adding some additional numbers, nice
    context 'When adding three and six' do
        it 'should equal nine' do
            expect(CoolThing.new().add_numbers(3, 6)).to eq 9
        end
    end
end
